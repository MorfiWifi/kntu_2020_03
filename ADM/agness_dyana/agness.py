"""
This Morfi wifi as always  aka morteza Eydipour (9811634)

This Might be Agnes implementation for a data set
(The only data set I Know is Iris !)
"""

from scipy.cluster.hierarchy import dendrogram, linkage
from sklearn.preprocessing import StandardScaler
import matplotlib.pyplot as plt
import time


def get_values_in_line(line):
    """seperate and Unique valies in line"""
    vals = []
    number = ''  # the readed number
    # for line in parts:
    for char in line:
        if char != " " and char != "," and char != "\n":
            number += char
        else:
            if number != '' and number != ' ' and number != '\n':
                vals.append(float(number))
            number = ''
            continue
    return vals


def convert_number_csv_array(path):
    """Convert Givent CSV data to Variable in CODE!"""
    total_vals = []
    file = open(path, "r")  # openning as text File
    for line in file:
        vals = get_values_in_line(line)
        total_vals.append(vals)

    return total_vals


def Agnes():
    """Algo for Running Agnes on Given data set (This Part of Cod is From sklearn samples Modified) """
    start_time = time.time()
    # Converting Iris Data set for Using in This Algo
    values = convert_number_csv_array("iris.csv")
    selected_clumns = []
    lables = []
    for cl1, cl2, cl3, cl4, cl5 in values:
        selected_clumns.append([cl1, cl2])
        lables.append(cl5)

    # Normalize Clumn values ! 
    selected_clumns = StandardScaler().fit_transform(selected_clumns)

    labels = range(0, len(selected_clumns))

    plt.figure(figsize=(10, 7))
    plt.subplots_adjust(bottom=0.1)
    plt.scatter(selected_clumns[:, 0], selected_clumns[:, 1], label='True Position')

    for label, x, y in zip(labels, selected_clumns[:, 0], selected_clumns[:, 1]):
        plt.annotate(
            label,
            xy=(x, y), xytext=(-3, 3),
            textcoords='offset points', ha='right', va='bottom')
    plt.show()

    linked = linkage(selected_clumns, 'single')

    plt.figure(figsize=(10, 7))
    dendrogram(linked,
               orientation='top',
               labels=labels,
               distance_sort='descending',
               show_leaf_counts=True)
    plt.show()

    end_time = time.time()
    print('Agnes Took : ', end_time - start_time, 'sec')

Agnes()
