from scipy.cluster.hierarchy import dendrogram, linkage
from sklearn.preprocessing import StandardScaler
import matplotlib.pyplot as plt

def readLine(line):
    vals = []
    number = ''
    for char in line:
        if char != " " and char != "," and char != "\n":
            number += char
        else:
            if number != '' and number != ' ' and number != '\n':
                vals.append(float(number))
            number = ''
            continue
    return vals


dataset = []
file = open("seeds_dataset.csv", "r")
for line in file:
    att = readLine(line)
    dataset.append(att)

columns = []
classes = []
for a1, a2, a3, a4, a5, a6, a7, cl in dataset:
    columns.append([a1, a2])
    classes.append(cl)

columns = StandardScaler().fit_transform(columns)

labels = range(0, len(columns))

plt.figure(figsize=(10, 7))
plt.subplots_adjust(bottom=0)
plt.scatter(columns[:, 0], columns[:, 1], label='True Position')

for label, x, y in zip(labels, columns[:, 0], columns[:, 1]):
    plt.annotate(
        label,
        xy=(x, y), xytext=(-3, 3),
        textcoords='offset points', ha='right', va='bottom')
plt.show()

linked = linkage(columns, 'single')

plt.figure(figsize=(10, 7))
dendrogram(linked,
            orientation='top',
            labels=labels,
            distance_sort='descending',
            show_leaf_counts=True)

plt.show()
