"""
Created by MorfiWifi aka moteza eydipour (9811634)
KNT Univercity
2020

morfi dose'nt like copy  so its diffrent """
import pandas as pd , time , numpy as np


atribte_list = ['a1', 'a2', 'a3', 'a4', 'a5', 'a6', 'a7', 'a8' , 'a9', 
                'a10', 'a11', 'a12', 'a13', 'a14', 'a15', 'a16' , 'a17'
                , 'a18' , 'a19' , 'a20'] ;

start_time = time.time() # starting CLOCK
database = pd.read_csv('Dataset.csv', sep=' ', names=atribte_list)
database = database.replace(np.nan, 0 ) # iwant no nan in my data ! 

#building my own atributes by Agrigating 
database['A'] = database['a1'] + database['a2'] + database['a3'] + database['a4']
database['B'] = database['a5'] + database['a6'] + database['a7'] + database['a8']
database['C'] = database['a9'] + database['a10'] + database['a11'] + database['a12']
database['D'] = database['a13'] + database['a14'] + database['a15'] + database['a16']
database['E'] = database['a17'] + database['a18'] + database['a19'] + database['a20']

database = database.drop(columns=atribte_list)# drop all Old Clumns 

sg_sum =  database.agg({'A' : ['sum'], 'B' : ['sum'] , 'C' : ['sum'] , 'D' : ['sum'] , 'E' : ['sum']})

#print(sg_sum)
#print(sg_sum['A']['sum'])
print('for 0 DIM ----------------------------')
clumns =  ['A' , 'B' , 'C' ,  'D' , 'E']
print ('{:10s} , {:10s} , {:10s} , {:10s} , {:10s} , {:10s}'.format('A','B','C','D','E','AGG' ))
for i in range(32):
    bi = list('{0:0b}'.format(i))
    while len(bi) < 5:
        bi.insert(0 , '0')
    res = "" 
    agg = 0
    for attr , con , index in zip( clumns, bi , range(5)):
        if con == '0':
            res +=  '{:10s}'.format( '0')
        else:
            res +=  '{:10s}'.format( '1')
            agg += sg_sum[attr]['sum']
        res += ' , '
    
    res += '{:10s}'.format( str(agg))
    print(res)
        
print('for 1 DIM ----------------------------')

print ('{:10s} , {:10s} , {:10s} , {:10s} , {:10s} , {:10s}'.format('A','B','C','D','E','AGG' ))
for k in range(5):
    print('for 1_', k+1 , '/ 5 ------------------------------')
    for i in range(32):
        bi = list('{0:0b}'.format(i))
        while len(bi) < 5:
            bi.insert(0 , '0')
        res = "" 
        agg = 0
        for attr , con , index in zip( clumns, bi , range(5)):
            if index == k:
                res +=  '{:10s}'.format( '*')
                #agg += sg_sum[attr]['sum']
            elif con == '0':
                res +=  '{:10s}'.format( '0')
            else:
                res +=  '{:10s}'.format( '1')
                agg += sg_sum[attr]['sum']
            res += ' , '
        
        res += '{:10s}'.format( str(agg))
        print(res)    
    
print ('{:10s} , {:10s} , {:10s} , {:10s} , {:10s} , {:10s}'.format('A','B','C','D','E','AGG' ))
counti = 0
for k1 in range(4):
    for k2 in range(k1+1 , 5):
        counti += 1   
        print('for 2_', counti , '/ 10 ------------------------------')
        for i in range(32):
            bi = list('{0:0b}'.format(i))
            while len(bi) < 5:
                bi.insert(0 , '0')
            res = "" 
            agg = 0
            for attr , con , index in zip( clumns, bi , range(5)):
                if index == k1 or index == k2:
                    res +=  '{:10s}'.format( '*')
                elif con == '0':
                    res +=  '{:10s}'.format( '0')
                else:
                    res +=  '{:10s}'.format( '1')
                    agg += sg_sum[attr]['sum']
                res += ' , '
            res += '{:10s}'.format( str(agg))
            print(res)  
            
print ('{:10s} , {:10s} , {:10s} , {:10s} , {:10s} , {:10s}'.format('A','B','C','D','E','AGG' ))            
counti = 0
for k1 in range(3):
    for k2 in range(k1+1 , 4):
        for k3 in range(k2+1 , 5):
            counti += 1   
            print('for 3_', counti , '/ 10 ------------------------------')
            for i in range(32):
                bi = list('{0:0b}'.format(i))
                while len(bi) < 5:
                    bi.insert(0 , '0')
                res = "" 
                agg = 0
                for attr , con , index in zip( clumns, bi , range(5)):
                    if index == k1 or index == k2 or index == k3:
                        res +=  '{:10s}'.format( '*')
                    elif con == '0':
                        res +=  '{:10s}'.format( '0')
                    else:
                        res +=  '{:10s}'.format( '1')
                        agg += sg_sum[attr]['sum']
                    res += ' , '
                res += '{:10s}'.format( str(agg))
                print(res)    
                 

print ('{:10s} , {:10s} , {:10s} , {:10s} , {:10s} , {:10s}'.format('A','B','C','D','E','AGG' ))            
counti = 0
for k1 in range(2):
    for k2 in range(k1+1 , 3):
        for k3 in range(k2+1 , 4):
            for k4 in range(k3+1 , 5):
                counti += 1   
                print('for 4_', counti , '/ 5 ------------------------------')
                for i in range(32):
                    bi = list('{0:0b}'.format(i))
                    while len(bi) < 5:
                        bi.insert(0 , '0')
                    res = "" 
                    agg = 0
                    for attr , con , index in zip( clumns, bi , range(5)):
                        if index == k1 or index == k2 or index == k3 or index == k4:
                            res +=  '{:10s}'.format( '*')
                        elif con == '0':
                            res +=  '{:10s}'.format( '0')
                        else:
                            res +=  '{:10s}'.format( '1')
                            agg += sg_sum[attr]['sum']
                        res += ' , '
                    res += '{:10s}'.format( str(agg))
                    print(res)  
    
print('for 5_', 1 , '/ 1 ------------------------------')
print ('{:10s} , {:10s} , {:10s} , {:10s} , {:10s} , {:10s}'.format('A','B','C','D','E','AGG' ))  
agg = 0
res = "" 
for attr , con , index in zip( clumns, bi , range(5)):
    res +=  '{:10s}'.format( '*')
    res += ' , '
    agg += sg_sum[attr]['sum']
res += '{:10s}'.format( str(agg))
print(res) 


print('took : ' , round((time.time() - start_time), 3)  , ' sec' )
