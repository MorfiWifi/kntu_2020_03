import math

# Example points in 3-dimensional space...
x = (1, 1, 1, 1)
y = (2, 2, 2, 2)
distance = math.sqrt(sum([(a - b) ** 2 for a, b in zip(x, y)]))
print("Euclidean distance from x to y: ", distance)

x = (0, 1, 0, 1)
y = (1, 0, 1, 0)
distance = math.sqrt(sum([(a - b) ** 2 for a, b in zip(x, y)]))
print("Euclidean distance from x to y: ", distance)

x = (0, -1, 0, 1)
y = (1, 0, -1, 0)
distance = math.sqrt(sum([(a - b) ** 2 for a, b in zip(x, y)]))
print("Euclidean distance from x to y: ", distance)

x = (1, 1, 0, 1, 0, 1)
y = (1, 1, 1, 0, 0, 1)
distance = math.sqrt(sum([(a - b) ** 2 for a, b in zip(x, y)]))
print("Euclidean distance from x to y: ", distance)

import math


def cosine_similarity(v1, v2):
    "compute cosine similarity of v1 to v2: (v1 dot v2)/{||v1||*||v2||)"
    sumxx, sumxy, sumyy = 0, 0, 0
    for i in range(len(v1)):
        x = v1[i];
        y = v2[i]
        sumxx += x * x
        sumyy += y * y
        sumxy += x * y
    return sumxy / math.sqrt(sumxx * sumyy)


# x = (1, 1, 1 , 1)
# y = (2, 2, 2 , 2)
v1, v2 = [1, 1, 0, 1, 0, 1], \
         [1, 1, 1, 0, 0, 1]
print(v1, v2, cosine_similarity(v1, v2))
#
# Output: [3, 45, 7, 2] [2, 54, 13, 15] 0.972284251712
import numpy as np
from sklearn.metrics import jaccard_score

print(jaccard_score(v1, v2) , 'jackard distance ?!')