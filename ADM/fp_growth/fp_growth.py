"""
Created by MorfiWifi aka moteza eydipour (9811634)
KNT Univercity
2020
morfi dose'nt like copy ! [ so it's different !!]

Implementation of Fp_growth Algo 
"""
import pandas as pd, time, numpy as np

MIN_SUP = 2  # min support For Building Tree and Calculateion


class Node:
    """Simple Nod for Building Tree"""

    def __init__(self, data, parent=-1, child=None, is_root=False):
        self.child = child  # list of Nodes
        self.parent = parent
        self.data = data
        self.count = 1
        self.is_root = is_root

    def __str__(self):
        """Override to String For Simple debug"""
        if self.child is not None:
            child_val = len(self.child)
        else:
            child_val = ' NONE '
        return str(self.data) + " : C= " + str(self.count) + " : ch_count: " + str(child_val)

    def print_visual(self, full=False, depth=0, file=None):
        """print Viually in CMD or Any Where!"""
        depth += 1
        if not full:
            parent_val = self.parent
            if file is not None:
                print('{:10s} , {:2d} , {:10s} , {:2d} , {:10s} , {:2d} , {:10s} , {:10d}'.format('depth :', depth,
                                                                                                  'Parent : ',
                                                                                                  parent_val,
                                                                                                  'value :', self.data,
                                                                                                  'Count: ',
                                                                                                  self.count),
                      file=file)
            else:
                print('{:10s} , {:2d} , {:10s} , {:2d} , {:10s} , {:2d} , {:10s} , {:10d}'.format('depth :', depth,
                                                                                                  'Parent : ',
                                                                                                  parent_val,
                                                                                                  'value :', self.data,
                                                                                                  'Count: ',
                                                                                                  self.count))
        else:
            if self.is_root:
                print(' --- ROOT --- ')
                for ch in self.child:
                    ch.print_visual(full=True, depth=depth)
            else:
                if self.parent == -1:
                    parent_val = "ROOT"
                else:
                    parent_val = self.parent
                print('Parent : ', parent_val, 'depth :', depth, 'value :', self.data, 'Count: ', self.count)
                for ch in self.child:
                    ch.print_visual(full=True, depth=depth)


def add_data(root, value):
    """Add Child Or Add to Count"""
    not_found = True
    if root.child is not None:
        for ch in root.child:
            if ch.data == value:
                ch.count += 1
                not_found = False

    if not_found:
        ch = Node(value, parent=root.data)
        if root.child is None:
            root.child = []
        root.child.append(ch)
    # return Changed child
    return ch


def get_values_in_line(line):
    """seperate and Unique valies in line"""
    vals = []
    number = ''  # the readed number
    for char in line:
        if char != " ":
            number += char
        else:
            if number != '' and number != ' ' and number != '\n':
                vals.append(int(number))
            number = ''
            continue
    vals = list(set(vals))  # make values Unique
    return vals


def order_values(values, orderer):
    """order line value's based or Best order"""
    new_list = []
    for o in orderer:
        if o in values:
            new_list.append(o)
    return new_list


def get_best_items():
    file = open("Dataset.txt", "r")  # openning as text File

    line_index = 0
    count = {}
    for line in file:
        vals = get_values_in_line(line)

        # this is for depth one!
        for item in vals:
            if item in count:
                count[item] += 1
            else:
                count[item] = 1

        # lismit max index (for Test ONLY)
        # if line_index > 50:
        #     break

    # filter by min supp
    new_count = {}
    for key in count:
        val = count[key]
        if val > MIN_SUP:
            new_count[key] = val
    count = new_count  # filtered

    # sort By Value
    count = {k: v for k, v in sorted(count.items(), key=lambda item: item[1], reverse=True)}

    file.close()
    return count


def build_tree_from_lines(root, best_order):
    """processe line by line throe data in file"""
    file = open("Dataset.txt", "r")  # openning as text File

    line_index = 0
    # count = {}
    for line in file:
        line_index += 1
        vals = get_values_in_line(line)
        vals = order_values(vals, best_order)

        # here we process the TREE
        nodi = root
        for val in vals:
            nodi = add_data(nodi, val)

        # limit Count Test ONLY
        # if line_index > 50:
        #     break

    file.close()


def bfs_throw_tree_print(root: Node, file=None):
    """I Guess Best Way to print Tree!"""
    stack = []
    depth = 1

    for ch in root.child:
        stack.append(ch)

    while len(stack) > 0:
        locs = []
        for c in stack:
            c.print_visual(full=False, depth=depth, file=file)
            if c.child is not None and len(c.child) > 0:
                locs.extend(c.child)

        stack = locs
        depth += 1


# this is Main --------------------
results = open('results.txt', 'w')

root = Node(-1, is_root=True)  # this is Only Root (start of Tree)
print(root)

best_keys = get_best_items()
build_tree_from_lines(root, best_keys)

bfs_throw_tree_print(root, results)
results.close()
