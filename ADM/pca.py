"""
Created by MorfiWifi aka moteza eydipour (9811634)
KNT Univercity
2020

morfi dose'nt like copy  Not When They Give me Only 3 days ! Who Do You Think We are ? Solders waiting for command ?
Thanks to GITHUB , sklearn ,  diegocavalca  ,  aakashrai1 , Rina Buoy , YouTube
-- should i Use Sklearning ? (I Guess I have no choice with this timing!)
"""

import copy, time
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.preprocessing import StandardScaler

dasets = ['iris.csv', 'abalone.csv', 'seeds_dataset.csv']
atribte_list = [['a1', 'a2', 'a3', 'a4', 'class'],
                ['class', 'a1', 'a2', 'a3', 'a4', 'a5', 'a6', 'a7', 'a8'],
                ['a1', 'a2', 'a3', 'a4', 'a5', 'a6', 'a7', 'class']]
class_atribute_index = [4, 0, 7]
class_names = [['Setosa', 'Versicolor', 'Virginica'],
               ['M', 'I', 'F'],
               [1, 2, 3]]

start_time = time.time()
for db_name, attrs, class_index, classes in zip(dasets, atribte_list, class_atribute_index, class_names):

    database = pd.read_csv(db_name, sep=',', names=attrs)

    value_attrs = copy.copy(attrs)
    value_attrs.pop(class_index)
    vector_size = len(value_attrs)


    attributes = database.loc[:, value_attrs]

    attributes = StandardScaler().fit_transform(attributes)  # normalize With mean 0 vaeianve 1 (standard!)

    mean = np.mean(attributes, axis=0)

    cov = (attributes - mean).T.dot((attributes - mean)) / (attributes.shape[0] - 1)

    eValues, eVec = np.linalg.eig(cov)

    eigenPairs = [(np.abs(eValues[i]), eVec[:, i]) for i in range(len(eValues))]
    eigenPairs.sort()
    eigenPairs.reverse()

    bestDims = np.hstack((eigenPairs[0][1].reshape(vector_size, 1), eigenPairs[1][1].reshape(vector_size, 1)))

    mul = attributes.dot(bestDims)

    pcaDF = pd.DataFrame(data=mul, columns=['pca1', 'pca2'])
    newDf = pd.concat([pcaDF, database[['class']]], axis=1)

    fig = plt.figure(figsize=(7, 7))
    axis = fig.add_subplot(111, facecolor='white')
    axis.set_xlabel('PCA 1', fontsize=12)
    axis.set_ylabel('PCA 2', fontsize=12)
    axis.set_title('PCA on ' + db_name, fontsize=15)

    for classVal, color in zip(classes, ['y', 'b', 'r']):
        indexVals = newDf['class'] == classVal
        axis.scatter(newDf.loc[indexVals, 'pca1'], newDf.loc[indexVals, 'pca2'], c=color, s=50)
    axis.legend(classes, loc="upper right")
    axis.grid(linewidth=0.5)
    fig.savefig('pca ' + db_name + '.png', dpi=200)

print('operation took : ', time.time() - start_time, ' second')
