"""
Created by MorfiWifi aka moteza eydipour (9811634)
KNT University 2020
-- Using Copy Right reverse law !
"""
import numpy as np
from sklearn.cluster import DBSCAN
from sklearn.preprocessing import StandardScaler
import matplotlib.pyplot as plt

def get_values_in_line(line):
    """seperate and Unique valies in line"""
    vals = []
    number = ''  # the readed number
    # for line in parts:
    for char in line:
        if char != " " and char != "," and char != "\n":
            number += char
        else:
            if number != '' and number != ' ' and number != '\n':
                vals.append(float(number))
            number = ''
            continue
    return vals


def convert_number_csv_array(path):
    """Convert Givent CSV data to Variable in CODE!"""
    total_vals = []
    file = open(path, "r")  # openning as text File
    for line in file:
        vals = get_values_in_line(line)
        total_vals.append(vals)

    return total_vals


def DbScan():
    """Algo for Runig DbScan on Given data set (This Part of Cod is From sklearn samples Modified) """
    values = convert_number_csv_array("iris.csv")
    selected_clumns = []
    lables = []
    for cl1, cl2, cl3, cl4, cl5 in values:
        selected_clumns.append([cl1, cl2])
        lables.append(cl5)

    selected_clumns = StandardScaler().fit_transform(selected_clumns)

    data_set = DBSCAN(eps=0.3, min_samples=10).fit(selected_clumns)
    core_samples_mask = np.zeros_like(data_set.labels_, dtype=bool)
    core_samples_mask[data_set.core_sample_indices_] = True
    labels = data_set.labels_

    number_clusters = len(set(labels)) - (1 if -1 in labels else 0)

    unique_labels = set(labels)
    colors = [plt.cm.Spectral(each)
              for each in np.linspace(0, 1, len(unique_labels))]
    for k, col in zip(unique_labels, colors):
        if k == -1:
            col = [0, 0, 0, 1]

        class_member_mask = (labels == k)

        xy = selected_clumns[class_member_mask & core_samples_mask]
        plt.plot(xy[:, 0], xy[:, 1], 'o', markerfacecolor=tuple(col),
                 markeredgecolor='k', markersize=14)

        xy = selected_clumns[class_member_mask & ~core_samples_mask]
        plt.plot(xy[:, 0], xy[:, 1], 'o', markerfacecolor=tuple(col),
                 markeredgecolor='k', markersize=6)

    plt.title('Clusters Calculated: %d' % number_clusters)
    plt.show()

DbScan()
