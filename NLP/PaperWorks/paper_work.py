"""
Copy Rights Morfi wifi AKA morteza Eydipout (9811634)
this is A Simple Implementation of Paper Only For 
research reasons -non Commercial usage
The Code Will be Updated Throw Time
"""

# --------------- main ------------------
import pandas as pd  # for building dataset from data sets
from sklearn.metrics.pairwise import cosine_similarity  # similarity calculation
from scipy import sparse  # vector space conversation
import sent2vec  # main library that we are talking about
import numpy as np


def start_sent2vec():
    model = sent2vec.Sent2vecModel()
    model.load_model('wiki_unigrams.bin')  # Loading Model

    """
    # ---------------------------------------------------------------
    # Reading Data set Simlex
    # ---------------------------------------------------------------
    """
    simlex_dataset = read_simlex999()
    count = 0
    total_diff = 0
    max_diff = 0
    min_diff = float('inf')
    total_dist = 0
    max_dist = 0
    min_dist = float('inf')
    for word1, word2, score in zip(simlex_dataset.word1,
                                   simlex_dataset.word2,
                                   simlex_dataset.SimLex999):
        score = float(score) / 10  # simlex score 0 - 10
        word1_matrix = model.embed_sentence(word1)
        word2_matrix = model.embed_sentence(word2)
        similarity = calculate_similarity(word1_matrix, word2_matrix)[0][1]
        distance = np.linalg.norm(word1_matrix - word2_matrix)
        dist = distance

        diff = abs(similarity - score)
        total_diff += diff
        total_dist += dist
        count += 1
        if diff > max_diff:
            max_diff = diff
        if diff < min_diff:
            min_diff = diff
        if dist > max_dist:
            max_dist = dist
        if dist < min_dist:
            min_dist = dist
    avg_dist = total_dist / count
    print('simlex-999 results')
    print('AVG diff:', total_diff / count, 'perfo:', 1 - (total_diff / count), 'max:', max_diff, 'min:', min_diff)
    print('AVG dist:', total_dist / count, 'perfo:', 1 - (avg_dist / (max_dist - min_dist)), 'max:', max_dist, 'min:',
          min_dist)

    """
        # ---------------------------------------------------------------
        # Reading Data set MEN
        # ---------------------------------------------------------------
    """
    men_dataset = read_men_dataset()
    count = 0
    total_diff = 0
    max_diff = 0
    min_diff = float('inf')
    total_dist = 0
    max_dist = 0
    min_dist = float('inf')
    for word1, word2, score in zip(men_dataset.word1,
                                   men_dataset.word2,
                                   men_dataset.score):
        score = float(score) / 50  # men score 0 - 50
        word1_matrix = model.embed_sentence(word1)
        word2_matrix = model.embed_sentence(word2)
        similarity = calculate_similarity(word1_matrix, word2_matrix)[0][1]
        diff = abs(similarity - score)
        total_diff += diff
        count += 1
        distance = np.linalg.norm(word1_matrix - word2_matrix)
        dist = distance
        total_dist += dist
        if diff > max_diff:
            max_diff = diff
        if diff < min_diff:
            min_diff = diff
        if dist > max_dist:
            max_dist = dist
        if dist < min_dist:
            min_dist = dist
    avg_dist = total_dist / count
    print("mens result")
    print('AVG diff:', total_diff / count, 'perfo:', 1 - (total_diff / count), 'max:', max_diff, 'min:', min_diff)
    print('AVG dist:', total_dist / count, 'perfo:', 1 - (avg_dist / (max_dist - min_dist)), 'max:', max_dist, 'min:',
          min_dist)
    """
            # ---------------------------------------------------------------
            # Reading Data set RareWords
            # ---------------------------------------------------------------
    """
    rarewords_dataset = read_rarewords()
    count = 0
    total_diff = 0
    max_diff = 0
    min_diff = float('inf')
    total_dist = 0
    max_dist = 0
    min_dist = float('inf')
    for word1, word2, score in zip(rarewords_dataset.word1,
                                   rarewords_dataset.word2,
                                   rarewords_dataset.f1):
        score = float(score) / 10  # rare word score 0 - 10
        word1_matrix = model.embed_sentence(word1)
        word2_matrix = model.embed_sentence(word2)
        similarity = calculate_similarity(word1_matrix, word2_matrix)[0][1]
        diff = abs(similarity - score)
        total_diff += diff
        count += 1
        distance = np.linalg.norm(word1_matrix - word2_matrix)
        dist = distance
        total_dist += dist
        if diff > max_diff:
            max_diff = diff
        if diff < min_diff:
            min_diff = diff
        if dist > max_dist:
            max_dist = dist
        if dist < min_dist:
            min_dist = dist
    avg_dist = total_dist / count
    print("rare words result  :")
    print('AVG diff:', total_diff / count, 'perfo:', 1 - (total_diff / count), 'max:', max_diff, 'min:', min_diff)
    print('AVG dist:', total_dist / count, 'perfo:', 1 - (avg_dist / (max_dist - min_dist)), 'max:', max_dist, 'min:',
          min_dist)


def calculate_similarity(Ematrix_w1, Ematrix_w2):
    """Calculate Similarity of Two Vectorized Word"""
    A_sparse = sparse.csr_matrix([Ematrix_w1[0], Ematrix_w2[0]])
    similarities = cosine_similarity(A_sparse)
    return similarities


def read_simlex999():
    """Using panda dataFrame to read simlex-999 dataset """
    atribte_list = ['word1', 'word2', 'POS', 'SimLex999', 'conc_w1',
                    'conc_w2', 'concQ', 'Assoc', 'SimAssoc333',
                    'SD']

    database = pd.read_csv('SimLex-999.txt', sep='\t', names=atribte_list)
    database = database.drop([0])  # drop Row 0 (containing Column names)
    return database


def read_rarewords():
    """Using panda dataFrame to read RareWords dataset """
    atribte_list = ['word1', 'word2', 'f1', 'n1', 'n2',
                    'n3', 'n4', 'n5', 'n6',
                    'n7', 'n8', 'n9', 'n10']

    database = pd.read_csv('rw.txt', sep='\t', names=atribte_list)
    return database


def read_men_dataset():
    atribte_list = ['word1', 'word2', 'score']
    database = pd.read_csv('Men_dataset.txt', sep=' ', names=atribte_list)
    return database


start_sent2vec()

