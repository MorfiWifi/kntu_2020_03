import numpy as np


class Gp:

    def start(pop, fit, Size):
        newParents = np.empty((Size, pop.shape[1]))
        for index in range(Size):
            maxFitt = np.where(fit == np.max(fit))
            maxFitt = maxFitt[0][0]
            newParents[index, :] = pop[maxFitt, :]
        return newParents

    def recombine(parnts, size):
        offSprings = np.empty(size)
        indexes = np.uint8(size[1] / 2)
        for i in range(size[0]):
            p1 = i % parnts.shape[0]
            p2 = (i + 1) % parnts.shape[0]
            offSprings[i, 0:indexes] = parnts[p1, 0:indexes]
            offSprings[i, indexes:] = parnts[p2, indexes:]
        return offSprings

    def mutate(offspring):
        shape = offspring.shape
        random_value = np.random.randint(low=0, high=2, size=shape)
        offspring = offspring * random_value
        return offspring

    def fitness(population):
        return np.sum(population, axis=1)


parentSizes = [15, 30, 60, 90]
instanceCount = [4, 16, 32, 64]
generations = 100
validations = 30
for parentSize in parentSizes:
    for instances in instanceCount:
        print("Parents : ", instances, "Size :", parentSize)
        valids = []
        sumFitness = 0
        for val_index in range(validations):
            equation_inputs = np.ones((1, parentSize))

            pop_size = (instances, parentSize)
            population = np.random.randint(low=0, high=1 + 1, size=pop_size)

            for generation in range(generations):
                fitness = Gp.fitness(population)

                parents = Gp.start(population, fitness, instances)

                crossIndex = int(parents.shape[0] / 2)  # half Cross

                offspring_crossover = Gp.recombine(parents, (crossIndex, parentSize))
                offspring_mutation = Gp.mutate(offspring_crossover)

                population[0:crossIndex, :] = parents[0:crossIndex]
                population[crossIndex:, :] = offspring_mutation

            fitness = Gp.fitness(population)
            best_index = np.where(fitness == np.max(fitness))
            best_fitness = fitness[best_index]
            final_best = best_fitness[0]

            if (best_fitness[0] > parentSize * 0.9):
                valids.append(1)

            sumFitness += final_best

        print("AVG Best :", sumFitness / validations)
        if len(valids) >= 30:
            print(instances, parentSize, generations)
