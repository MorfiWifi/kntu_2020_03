"""
This Morfi wifi as always  aka morteza Eydipour (9811634)
here we want to optimize some parameters using simgle GA (genetic algorithm)
so just Think to it as simple problem
- Parameter Tuning !
(Using Wingsuit (No Enough RAM for pycharm any more))
unfortunetly this time it has to work 
- I need Some Grades !! (maybee no time to fix old ones...)

"""
import numpy as np


def sum_fitness(inputs, population):
    """Summ totall fitness of Genoms"""
    return np.sum(population * inputs, axis=1)


class Pool:
    """simple pool as Always
    this is all we have As GA algorithm !
    Noting More !!
    Other Thisg Considerd as User defined Objective and evaluatuion!!
    """

    def initiate(population, fitness, parents_count):
        """Constructor for POOL"""
        parents = np.empty((parents_count, population.shape[1]))
        for parent_num in range(parents_count):
            max_fitness = np.where(fitness == np.max(fitness))
            max_fitness = max_fitness[0][0]
            parents[parent_num, :] = population[max_fitness, :]
            fitness[max_fitness] = float('-inf')
        # self.parents = parents
        return parents

    def cross_over(parents, size):
        """Cross over on parents (how many resulst you want? = size)"""
        off = np.empty(size)
        indexes = np.uint8(size[1] / 2)
        for i in range(size[0]):
            p1 = i % parents.shape[0]
            p2 = (i + 1) % parents.shape[0]
            off[i, 0:indexes] = parents[p1, 0:indexes]
            off[i, indexes:] = parents[p2, indexes:]
        return off

    def mutate(offspring):
        """mutation fially!"""
        for index in range(offspring.shape[0]):
            random_value = np.random.uniform(0, 1000, 1)
            offspring[index, 4] = offspring[index, 4] + random_value
        return offspring


def is_one_out_goal(best_result, size):
    """Check if Geen is Near 1111 .... (All one in binary!)"""
    pass


def test_one_max():
    """All Genoms will be 1 ?!"""
    print('ONE MAX STARTED ----------------------')
    one_value = 999999
    geen_sizes = [15, 30, 60, 90]
    populations = [4, 6, 8, 10, 16, 18, 24, 30, 36, 40, 60, 70]
    num_generations = 10000
    validation_count = 30

    for geen_size in geen_sizes:
        should_be_res = (geen_size - 1) * one_value
        for parent_size in populations:
            validated_ones = []
            for val_index in range(validation_count):
                equation_inputs = np.ones((1, geen_size))
                num_weights = geen_size

                pop_size = (parent_size, num_weights)
                new_population = np.random.uniform(low=1, high=one_value, size=pop_size)

                for generation in range(num_generations):
                    fitness = sum_fitness(equation_inputs, new_population)

                    parents = Pool.initiate(new_population, fitness, parent_size)
                    # cross Over Center!
                    offspring_crossover = Pool.cross_over(parents, (int(parents.shape[0] / 2), num_weights))
                    offspring_mutation = Pool.mutate(offspring_crossover)

                    new_population[0:int(parents.shape[0] / 2), :] = parents[0:int(parents.shape[0] / 2)]
                    new_population[int(parents.shape[0] / 2):, :] = offspring_mutation

                fitness = sum_fitness(equation_inputs, new_population)
                best_index = np.where(fitness == np.max(fitness))
                best_fitness = fitness[best_index]
                final_best = best_fitness[0]
                diff = should_be_res - final_best

                if ((should_be_res - best_fitness[0]) < one_value * 0.1):
                    """less than 10% of Max Value Diffrence in total"""
                    validated_ones.append(1)

            if len(validated_ones) >= 30:
                print("for popupation ", parent_size, 'geen size ', geen_size, 'generations ', num_generations,
                      " ONE MAX WORKED")
            else:
                print('**DIDNT WORK ', "for popupation ", parent_size, 'geen size ', geen_size, 'generations ',
                      num_generations)


def test_trap_5(k, inputi):
    """5 is atrap just Go fare  more ! """
    if inputi == k:
        return k
    return (k - 1) - inputi


def main():
    """this is The main Function (main thread in project)"""
    print('main Function started ......')
    test_one_max()
    print('main function finished')


main()
