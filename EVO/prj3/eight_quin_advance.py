"""
Created by MorfiWifi aka moteza eydipour (9811634)
KNT Univercity
2020
morfi dose'nt like copy ! [ so it's different !!]

so ites Time Work Consistant ON what You Preserve As Job for Better Foture 
and more Relayable Value for Resulting in Final Term !
The Projects Are Too much but this is One of Only chances that you have 
for fixing Almost Every Thing ... but I Know it takes alot of efford
to do this as You are Weak and Tired of Every Thing so be Carefull
and Do what Ever You will because there is No such thing as result
for What We DO !!
"""
import random, copy

# Initiali Values As Project Settings
mutation_chance = 1  # swap _only valid format
selection_chance = 0.5  # chance of choices for next Gen
base_identity = 1


def genom_builder(length: int):
    """Build random valid Genome (Quins Place in Row / Column)
     This Solution is In Reduced Complexity space !
     by Simple Heuristic (Two Quin Can't be in same row/column)
     """
    baseItem = []
    genom = []
    for i in range(1, length + 1):
        baseItem.append(i)

    while len(baseItem) > 0:
        rand = random.randint(0, len(baseItem) - 1)
        genom.append(baseItem[rand])
        baseItem.pop(rand)

    return genom


def remove_redundency(input):
    """Gol bless Internet / its a good Thing !"""
    seen = set()
    seen_add = seen.add
    return [x for x in input if not (x in seen or seen_add(x))]


def get_identity():
    """returns a number that Increases Over Time by 1
    we dont Use Multi Thread processing So yet ther is No problem
    about this Cod
    """
    global base_identity
    base_identity += 1
    return base_identity


class Genom:
    def __init__(self, length: int, age=0):
        self.size = length
        self.data = genom_builder(length)
        self.cross_count = self.get_cross_count()
        self.save_finess_base()
        self.age = age;  # for Using Age Base Selection
        self.id = get_identity();  # this is For IdenTifying The real Best!(Used in Compariosn)
        self.fitness = 0;

        if not self.is_valid():
            print('i am not valid!')

    def is_valid(self):
        """Check if Gen is well formatted or not
        all Numbers in range size should be included"""
        for i in range(1, self.size + 1):
            if i not in self.data:
                return False
        return True

    def get_cross_count(self):
        """Return Number of Crosses / conflicts
            Use This as Private Function to improve
            perfomance
        """

        count = 0
        for i in range(self.size):
            quin1 = {"x": i, "y": self.data[i]}
            for j in range(self.size):
                if i == j:
                    continue  # ignore Compare with self
                quin2 = {"x": j, "y": self.data[j]}

                # Check if There is Cross ----
                # same row or clumn
                if abs(quin1['x'] - quin2['x']) == 0 \
                        or abs(quin1['y'] - quin2['y']) == 0:
                    count += 1
                    continue

                dif = abs(quin1['x'] - quin2['x']) + \
                      abs(quin1['y'] - quin2['y'])
                if (dif == 1):  # near cells
                    count += 1
                    continue
                dif = abs(quin1['x'] - quin2['x']) - \
                      abs(quin1['y'] - quin2['y'])

                if (dif == 0):  # X-cross cells
                    count += 1
                    continue

        self.cross_count = count
        return count

        # for j in range(i + 1 , self.size):

    def save_finess_base(self):
        """calculate finess with Basic Fuction
        and save it
        1/1+(SUM(numberOfCruses))
        """
        self.fitness = 1 / (1 + self.cross_count);

    def get_fitnes(self):
        """ simple return off the fitness"""
        return self.fitness

    def mutate(self):
        """with A chance will swap Two Genoms
        remember this mutation is Swap ONLY
        """
        wheele_value = random.randint(0, 100)
        if wheele_value < mutation_chance * 100:  # doo Mutation by 80%
            st_index = random.randint(0, self.size - 1)
            end_index = random.randint(0, self.size - 1)

            temp = self.data[st_index]
            self.data[st_index] = self.data[end_index]
            self.data[st_index] = temp


class Pool:
    """ This class is The base for Running / Working with Population !
    I think this would  be Good
    """

    def distance_to_best(self, gen):
        """claclulate Distance between This Genom Till The Best One
        More Distance -> less Reduction from Fitness !
        This is For Preventing population Take Over With Single geen
        Fitness Sharing (some HOW!)
        
        """
        distanec = 0;
        for index in range(len(gen.data)):
            distanec += abs(self.best.data[index] - gen.data[index])
        return distanec

    def __init__(self, size: int, length: int):
        """Initialize Pool with Pre set population
         @:param size = Count Of parents
         @:param length = Length of Genome / Count of Quins
         """
        self.best = None  # this is The Best Child
        self.size = size
        self.length = length
        self.parents = []  # array of patents
        for i in range(size):
            gin = Genom(length)
            gin.save_finess_base()
            self.parents.append(gin)

    def sort_parents(self):
        """Just Sorts parets based on Fitness (max First)"""
        sorted_parents = []

        st_index = 0
        end_index = len(self.parents)
        while st_index < end_index:
            fitness = float("-inf")
            choosen_inedx = -1
            for i in range(len(self.parents)):
                gin = self.parents[i]
                # gin = Genom(5)

                if fitness < gin.get_fitnes():
                    fitness = gin.get_fitnes()
                    choosen_inedx = i

            sorted_parents.append(copy.deepcopy(self.parents[choosen_inedx]))
            self.parents.pop(choosen_inedx)  # remove this index
            st_index += 1

        self.parents = sorted_parents
        return sorted_parents  # just return STH

    def print_parets(self):
        for i in range(len(self.parents)):
            gin = self.parents[i]
            gin.print();

    def is_finished(self):
        """checks if This poll has reached its Goal"""
        self.sort_parents()
        return self.parents[0].get_fitnes() == 1

    def save_best_genom(self):
        """set best value to parent/child with best Fitness(max)"""
        self.sort_parents()
        self.best = self.parents[0]

    def get_best_genom(self):
        """:returns The best Genom from data (parent) in this pool instance"""
        return self.best

    def crossover_genoms(self, g1: Genom, g2: Genom) -> [Genom]:
        """:returns Two new Genomes Crossed Over !
        Too Complicated Way to do cross over I don't even know
        how could i find this solution!
        """
        cross_index = random.randint(0, self.length - 1)

        # print('Cross index : ', cross_index)

        gg1 = Genom(g1.size)
        gg2 = Genom(g1.size)

        dd1 = []
        dd2 = []

        lg1 = g1.data[0:cross_index + 1]
        rg2 = g2.data[cross_index:g2.size]
        rg1 = g1.data[cross_index:g2.size]
        lg2 = g2.data[0:cross_index + 1]

        dd1.extend(lg1)
        dd1.extend(rg2)
        dd1.extend(rg1)
        dd1.extend(lg2)

        dd2.extend(lg2)
        dd2.extend(rg1)
        dd2.extend(rg2)
        dd2.extend(lg1)

        # print('dd1', dd1, ': dd2', dd2)
        dd1 = remove_redundency(dd1)
        dd2 = remove_redundency(dd2)

        gg1.data = dd1
        gg2.data = dd2

        # Force Recalculate Cross Count
        gg1.get_cross_count()
        gg2.get_cross_count()

        return [gg1, gg2]

    def build_offsprings(self, hasMutation=False, age=0):
        """
        build offspring form Sorted parents (assume they are sorted)
        :return: Nothing
        """
        child = []
        for i in range(0, len(self.parents) - 1, 2):
            two_child = self.crossover_genoms(self.parents[i], self.parents[i + 1])
            child.extend(two_child)

        if hasMutation:  # in case Do Mutation / Works by chance!
            for boy in child:
                boy.age = age + 1  # One Year Older Than Currnet Age (More is Younger)
                boy.mutate()
                boy.get_cross_count()  # re calc / if muted!
                boy.save_finess_base()

        # add all Child to parents (real pool)
        # now Prents.size = 2*size
        self.parents.extend(child)

    def select_new_generation_tournoment(self):
        """Select New Generation form Merged Child And parents
        At time of calling this Sized of parents array should be self.size * 2
        """

        # distributing based on Distance (We Like Distance)
        # not Toomuch distance but Not Too Few
        for p in self.parents:
            if p.id == self.best.id:
                continue
            distance = self.distance_to_best(p)
            if distance < 2:  # too close reduce chance
                p.fitness = p.fitness * 0.8
            if distance > 6:  # too far give chande
                p.fitness = p.fitness * 1.1


        # Add Age Good Thing ! Youns has more Chance !(Age Base)
        for p in self.parents:
            p.fitness = (1 + (p.age / 1000)) * p.fitness  # increase fitnss a bit

        self.sort_parents();

        self.sort_parents()  # first sort them
        choosen_ones = []
        choose_index = 0
        while len(choosen_ones) < self.size:
            choose_index = choose_index % self.size  # Dont let Out Of range
            wheel = random.randint(0, 100) < selection_chance * 100
            if wheel:
                choosen_ones.append(copy.deepcopy(self.parents[choose_index]))
            choose_index += 1

        self.parents = choosen_ones  # Only Use choosen

    def select_new_generation_scaling(self):
        """selecting partens for being parent!
        based on Fitness With Scaling Method Over Total Finess
        """
        # ee calculate Fitness base for parents(child won't change)
        for p in self.parents:
            p.save_finess_base()

        summ_fitness = 0.0001  # for non zero Device (shouldnt happen!)
        min_fitness = float('-inf')
        max_fitness = float('+inf')
        # at the End didn't use Beta Scaling (just Simple)
        for p in self.parents:
            if (p.fitness > max_fitness):
                max_fitness = p.fitness
            if (p.fitness < min_fitness):
                max_fitness = p.fitness
            summ_fitness += p.fitness

        self.save_best_genom()  # auto sort included

        # make fitnesses smooth!
        # Add Age Good Thing ! Youns has more Chance !(Age Base)
        for p in self.parents:
            p.fitness = p.fitness / summ_fitness
            p.fitness = (1 + (p.age / 1000)) * p.fitness  # increase fitnss a bit

        # distributing based on Distance (We Like Distance)
        # not Toomuch distance but Not Too Few
        for p in self.parents:
            if p.id == self.best.id:
                continue
            distance = self.distance_to_best(p)
            if distance < 2:  # too close reduce chance
                p.fitness = p.fitness * 0.8
            if distance > 6:  # too far give chande
                p.fitness = p.fitness * 1.1


        self.sort_parents();

        choosen_ones = []
        choose_index = 0
        while len(choosen_ones) < self.size:
            choose_index = choose_index % self.size  # Dont let Out Of range
            wheel = random.randint(0, 100) < selection_chance * 100
            if wheel:
                choosen_ones.append(copy.deepcopy(self.parents[choose_index]))
            choose_index += 1

        self.parents = choosen_ones  # Only Use choosen        

    def get_avg_fitness(self):
        value = 0.0  # cat ?
        for i in range(len(self.parents)):
            value = value + self.parents[i].get_fitnes()

        return value / len(self.parents)

    def get_min_max_fitness(self):
        v_min = float('+inf')
        v_max = float('-inf')
        for i in range(len(self.parents)):
            self.parents[i].save_finess_base()
            v_min = min(self.parents[i].get_fitnes(), v_min)
            v_max = max(self.parents[i].get_fitnes(), v_max)

        return [v_min, v_max]


class Stats:
    def __init__(self, name, reps):
        self.size = name
        self.res = []
        self.maxes = []
        self.mins = []
        self.avgs = []
        self.finished_in = []
        self.failed_count = 0
        self.rep_count = reps
        self.took = []

    def print_stats(self):
        print(" --- RESULT FOR ", self.size, " REP= ", self.res, " --- ")
        print('MAXES : ', self.maxes)
        print('MINS : ', self.mins)
        print('AVG : ', self.avgs)
        print('Failed Count : ', self.failed_count)
        print('finished in rep : ', self.finished_in)
        print('TOOKS : ', self.took)
        print('res', self.res)


# main -----------------------
import time

quinss = [8, 16]
for quins in quinss:
    for size in [16, 8]:
        status = Stats(size, 25)  # Object For saving Rsults ..
        for iteration in range(25):  # 25 time for evey item!
            start_time = time.time()  # start Time -----
            poool = Pool(size, quins)
            poool.sort_parents()  # cant say finished without sorting!
            avg = poool.get_avg_fitness()
            minmax = poool.get_min_max_fitness()
            # print('Pool AVG fit', avg, ' min ', minmax[0], ' max', minmax[1])
            # poool.print_parets()
            repeat = 25
            repeat_max = 25
            while not poool.is_finished() and repeat > 0:
                poool.build_offsprings(True, age=repeat_max - repeat)
                # poool.print_parets()
                poool.sort_parents()

                # poool.select_new_generation_tournoment()
                poool.select_new_generation_scaling()

                # poool.print_parets()
                # poool.sort_parents()  # cant say finished without sorting!
                poool.save_best_genom()  # this Sorts parents too
                avg = poool.get_avg_fitness()
                minmax = poool.get_min_max_fitness()
                # print('Pool AVG fit', avg, ' min ', minmax[0], ' max', minmax[1])
                repeat -= 1

            if repeat <= 0:
                status.failed_count += 1
                status.finished_in.append(25)
                # print("took: ", (time.time() - start_time), " seconds  for size ", size, ' Failded in ', 100, ' rep')
            else:
                status.finished_in.append(25 - repeat)
                # print("took: ", (time.time() - start_time), " seconds  for size ", size, 'don in ', 100 - repeat, ' rep')

            status.avgs.append(avg)
            status.maxes.append(minmax[1])
            status.mins.append(minmax[0])
            status.took.append((time.time() - start_time))

        status.print_stats()
