"""
Created by MorfiWifi aka moteza eydipour (9811634)
KNT Univercity
2020
morfi dose'nt like copy ! [ so it's different !!]


 - for Ease of Use ONLY HOLE can move around ! Other pieces won't do anything!
 - parrent has no Cross Over !
 - every single parent can Produce Child
 - for smooth result every selected parent will produce 2 childs
 - mutation is Choosing Random Move from List Of valid Moves !
 - saving inital stae of puzzle is Only for fast forward / we always can do moves in vers form last possition
 - parents size will increase over time ! but we have limit
 - child is as same as parent (as I Think of ! parents may mess up things!)
 - parent who generate child should be removed!! (it useless to save!)
 
 -Hope Some one will read this after all ...

Guess I am making thing Way more Complex than they are in riality!!
Hope I can solv this Correcly without tons of bugs !!

 how should i represent this ? 🤔 what is the Simplesest and easy ?..
"""
import random, time , copy

MAX_GENERATION = 200
MAX_POOL_SIZE = 40
MUTATION_CHANCE = 0.4
PUZZLE_SIZE = 9
MOEVS = {'L': -1, 'R': 1, 'U': -3, 'D': 3}
MOEVS_INV = {-1: 'L', 1: 'R', -3: 'U', 3: 'D'}
GOAL_LOCATIONS = {'*' : 0 , '1' : 1 , '2' : 2 , '3' : 3 , '4' : 4 , '5' : 5 ,'6' : 6 , '7' : 7  ,'8' :8}
GOAL_ARRAY = ['*','1','2','3','4','5','6','7','8']


def is_same(p1, p2):
    """Check if Two puzzle are the same """
    for i in len(p1.stattus):
        if p1.stattus[i] != p2.stattus:
            return False
    return True


class puzzle:
    """this can be called Genom some how! represents tha Game Totality as 
    simple Object with Simple fields (minimal property)"""
    def __init__(self, use_set= False , elements = None ,is_random=False, pre_moves=0):
        """build Puzzle Instance if is_random
        Will be Created as random state!
        pre moves : moves Don till here
        """
        self.cursor = 0  # index of * in array! (main Idea)
        self.stattus = ['*', '1', '2', '3', '4', '5', '6', '7', '8']  # arry of items!
        self.start_state = []  # start_attay
        self.history = []  # list of Don Moves
        self.move_count = pre_moves

    def get_valid_moves(self):
        """:returns list of valid moves [1,2,3,4] or [left,right,..]"""
        valid_moves = []

        x_index = self.cursor % 3
        y_index = int(self.cursor / 3)

        if x_index == 1:
            valid_moves.append('L')
            valid_moves.append('R')

        if x_index == 0:
            valid_moves.append('R')

        if x_index == 2:
            valid_moves.append('L')

        if y_index == 1:
            valid_moves.append('U')
            valid_moves.append('D')

        if y_index == 0:
            valid_moves.append('D')

        if y_index == 2:
            valid_moves.append('U')

        return valid_moves

    def base_fitness_calculator(self , elements):
        """ calculats fitness base on givven array!"""
        total_distance = 0
        for i in range(PUZZLE_SIZE):
            item = elements[i]
            total_distance += abs(GOAL_LOCATIONS[item] - i)
        return 1 / (1 + total_distance)        
    
    def get_fitnes(self):
        """:return fitness of This Pozzle instance"""
        return self.base_fitness_calculator(self.stattus)

    def apply_move(self , move):
        """apply move on currnt Puzzle"""
        self.apply_move_on_array(move , self.stattus)
        self.history.append(move)
        self.move_count += 1
        self.cursor = self.stattus.index('*')#update * possition
        
        
    def apply_move_on_array(self , move , array):
        """swap desiered elements"""
        cursor_index = array.index('*')
        index_diff = MOEVS[move]
        array[cursor_index] = array[cursor_index + index_diff]
        array[cursor_index+ index_diff ] = '*'
        
        
    def get_move_fitness(self, moves):
        """:returns the fitnes for Evey move
        can use After Move Fitness - Current Fitness
        gets list of Moves as in put
        """
        fitnesses = []
        for move in moves:
            temp = copy.deepcopy(self.stattus)
            self.apply_move_on_array(move , temp)
            fitnesses.append(self.base_fitness_calculator(temp))
        return fitnesses

    def get_as_string(self):
        """Sinle Line the Puzzle as One String """
        res = ""
        for i in range(len(self.stattus)):
            res += self.stattus[i]
        return res
    def deep_copy(self):
        """deep copy self (clone)"""
        pp = puzzle()
        pp.cursor = copy.copy(self.cursor)
        pp.stattus = copy.deepcopy(self.stattus)
        pp.start_state = copy.deepcopy(self.start_state)
        pp.history = copy.deepcopy(self.history)
        pp.move_count = copy.copy(self.move_count)
        return pp
        
    def get_child(self):
        """build child of this One!"""
        res = []
        valid_moves = self.get_valid_moves()
        for move in valid_moves:
            child = self.deep_copy()
            child.apply_move(move)
            res.append(child)
        return res
    
    def print(self):
        """print self as int in Consule"""
        print(self.get_as_string() ,' MC: ' , self.move_count)
    def is_finished(self):
        """chech if is finished ? or not """
        for elemnt , gg  in zip( self.stattus , GOAL_ARRAY):
            if not (elemnt == gg):
                return False
        return True
                
class pool:
    """ my Favorit Class For Handling Whole Ground of EVO algorithm"""
    def __init__(self):
        """ Initialize Pool For algorithm RUN"""
        self.parents = []
        self.generations = 0 #count of re offspring
    def remove_weak_parents(self):
        """Parents that has same State but One of Tham has More or EQ Move Count
          Will be removed from parent (data) set
          """
        size = len(self.parents)
        choosen_incexes = []
        # sort by move counts
        for i in range(size):
            for j in range(i + 1, size, 1):
                if self.parents[i].move_count > self.parents[j].move_count:
                    temp = self.parents[i]
                    self.parents[i] = self.parents[j]
                    self.parents[j] = temp

        # sort by puzzle map (STR)
        for i in range(size):
            for j in range(i + 1, size, 1):
                if self.parents[i].get_as_string() > self.parents[j].get_as_string():
                    temp = self.parents[i]
                    self.parents[i] = self.parents[j]
                    self.parents[j] = temp

        # choose non Redundent
        prv = "NOTHING!"
        for i in range(size):
            str_this = self.parents[i].get_as_string()
            if str_this == prv:
                continue
            else:
                choosen_incexes.append(i)

        # update parents
        temp_choosen = []
        for i in choosen_incexes:
            temp_choosen.append(self.parents[i])

        self.parents = temp_choosen
        return choosen_incexes
    def sort_by_fitness(self):
        """simple swap for sorting parents base on their fitness
        do this after a while in algo !
        """
        #sort by move count (min First)
        for i in range(len(self.parents) -1):
            for j in range(i +1, len(self.parents) , 1):
                if self.parents[i].move_count > self.parents[j].move_count:
                    temp = self.parents[i]
                    self.parents[i] = self.parents[j]
                    self.parents[j] = temp        
        
        
        #sot by fitness (max first)
        for i in range(len(self.parents) -1):
            for j in range(i +1, len(self.parents) , 1):
                if self.parents[i].get_fitnes() < self.parents[j].get_fitnes():
                    temp = self.parents[i]
                    self.parents[i] = self.parents[j]
                    self.parents[j] = temp
        pass
    
    def build_off_sptring(self):
        """build child for evey single parent"""
        self.generations += 1
        count_parents = len(self.parents)
        offs = []
        for parent in self.parents:
            child =  parent.get_child()
            offs.extend(child)
        # remove Old parents (They are Useless)
        self.parents.extend(offs)
        self.parents = self.parents[count_parents:]
        return offs
    def limit_parents_count(self):
        """remember parent max count has limitation
        might use some random in limitation (like Selection!)
        theey should be sorted first
        """
        if len(self.parents) < MAX_POOL_SIZE:
            return 'No Operation needed'
        while len(self.parents) > MAX_POOL_SIZE:
            self.parents.pop() # remove least fitness
        return 'Operation Don'
            
        
    def is_finished(self):
        """chek if this pool is finished 
        maning : has a parent with fitness = 1 
        or is_finished parent!
        sort parents first 
        """
        if len(self.parents) == 0:
            print("WTF : no parent to say finished or NOT !!")
            return False
        if self.parents[0].is_finished():
            return True
        return False        
                
    def print(self):
        """print fitness of all parets as single line!"""
        res = ''
        for parent in self.parents:
            res += str(round(parent.get_fitnes() , 3) ) + ' , '
        print(res)
# main ----------------------------------
def main():
    """define main as Seperated Function
    So i can collapse it (it take alot of space !)
    """
    print('Starting Main --------------------')
    start_time = time.time()
    
    p = puzzle()

    p.cursor = 4;
    p.stattus = [ '1', '2', '5','6',  '*', '8', '4',  '3', '7'];
    
    pp = pool()
    pp.parents.append(p)
    
    best_child = None
    while (not pp.is_finished() ) and (pp.generations < MAX_GENERATION):
        #pp.print()
        pp.build_off_sptring()
        pp.remove_weak_parents() #remove redundant(more depth parents)
        pp.sort_by_fitness()
        pp.limit_parents_count() #selection in parents
        best_child = pp.parents[0]
        print('parents count :' , len(pp.parents))
        pp.print()
        
    print('poo finished in ' , pp.generations ,' generations')
    if not best_child ==  None:
        print('child hist : ' , best_child.get_as_string() , ' hist: ' ,best_child.history , ' fit: ' , round(best_child.get_fitnes() , 3) )
    
    print('took : ' , round((time.time() - start_time), 3)  , ' sec' )
    #end of main Function ---------------------------------------------
    
main()
