"""
Created by MorfiWifi aka moteza eydipour (9811634)
KNT Univercity
2020
morfi dose'nt like copy ! [ so it's different !!]


 - for Ease of Use ONLY HOLE can move around ! Other pieces won't do anything!
 - parrent has no Cross Over !
 - every single parent can Produce Child
 - for smooth result every selected parent will produce 2 childs
 - mutation is Choosing Random Move from List Of valid Moves !
 - saving inital stae of puzzle is Only for fast forward / we always can do moves in vers form last possition
 - parents size will increase over time ! but we have limit
 - child is as same as parent (as I Think of ! parents may mess up things!)
 - parent who generate child should be removed!! (it useless to save!)
 
 -Hope Some one will read this after all ...

Guess I am making thing Way more Complex than they are in riality!!
Hope I can solv this Correcly without tons of bugs !!

 how should i represent this ? 🤔 what is the Simplesest and easy ?..
---
 12
345
678
---
"""
import random, time , copy ,keyboard  ,os

MAX_GENERATION = 200
MAX_POOL_SIZE = 40
MUTATION_CHANCE = 0.4
PUZZLE_SIZE = 9
MOEVS = {'L': -1, 'R': 1, 'U': -3, 'D': 3}
MOEVS_INV = {-1: 'L', 1: 'R', -3: 'U', 3: 'D'}
GOAL_LOCATIONS = {'*' : 0 , '1' : 1 , '2' : 2 , '3' : 3 , '4' : 4 , '5' : 5 ,'6' : 6 , '7' : 7  ,'8' :8}
GOAL_ARRAY = ['*','1','2','3','4','5','6','7','8']

# MOVES = ['left' , 'right' , 'up' , 'down']
# MOVES = [0 , 1 , 2 , 3]

        
def is_same(p1, p2):
    """Check if Two puzzle are the same """
    for i in len(p1.stattus):
        if p1.stattus[i] != p2.stattus:
            return False
    return True


class puzzle:
    """this can be called Genom some how! represents tha Game Totality as 
    simple Object with Simple fields (minimal property)"""
    def __init__(self, use_set= False , elements = None ,is_random=False, pre_moves=0):
        """build Puzzle Instance if is_random
        Will be Created as random state!
        pre moves : moves Don till here
        """
        self.cursor = 0  # index of * in array! (main Idea)
        self.stattus = ['*', '1', '2', '3', '4', '5', '6', '7', '8']  # arry of items!
        self.start_state = []  # start_attay
        self.history = []  # list of Don Moves
        self.move_count = pre_moves

    def get_valid_moves(self):
        """:returns list of valid moves [1,2,3,4] or [left,right,..]"""
        valid_moves = []

        x_index = self.cursor % 3
        y_index = int(self.cursor / 3)

        if x_index == 1:
            valid_moves.append('L')
            valid_moves.append('R')

        if x_index == 0:
            valid_moves.append('R')

        if x_index == 2:
            valid_moves.append('L')

        if y_index == 1:
            valid_moves.append('U')
            valid_moves.append('D')

        if y_index == 0:
            valid_moves.append('D')

        if y_index == 2:
            valid_moves.append('U')

        return valid_moves

    def base_fitness_calculator(self , elements):
        """ calculats fitness base on givven array!"""
        total_distance = 0
        for i in range(PUZZLE_SIZE):
            item = elements[i]
            total_distance += abs(GOAL_LOCATIONS[item] - i)
        return 1 / (1 + total_distance)        
    
    def get_fitnes(self):
        """:return fitness of This Pozzle instance"""
        return self.base_fitness_calculator(self.stattus)

    def apply_move(self , move):
        """apply move on currnt Puzzle"""
        valid_moves = self.get_valid_moves()
        if  not move in valid_moves:
            print('in valid move')
            return False
        
        self.apply_move_on_array(move , self.stattus)
        self.history.append(move)
        self.move_count += 1
        self.cursor = self.stattus.index('*')#update * possition
        
        
    def apply_move_on_array(self , move , array):
        """swap desiered elements"""
        cursor_index = array.index('*')
        index_diff = MOEVS[move]
        array[cursor_index] = array[cursor_index + index_diff]
        array[cursor_index+ index_diff ] = '*'
        
        
    def get_move_fitness(self, moves):
        """:returns the fitnes for Evey move
        can use After Move Fitness - Current Fitness
        gets list of Moves as in put
        """
        fitnesses = []
        for move in moves:
            temp = copy.deepcopy(self.stattus)
            self.apply_move_on_array(move , temp)
            fitnesses.append(self.base_fitness_calculator(temp))
        return fitnesses

    def get_as_string(self):
        """Sinle Line the Puzzle as One String """
        res = ""
        for i in range(len(self.stattus)):
            res += self.stattus[i]
        return res
    def deep_copy(self):
        """deep copy self (clone)"""
        pp = puzzle()
        pp.cursor = copy.copy(self.cursor)
        pp.stattus = copy.deepcopy(self.stattus)
        pp.start_state = copy.deepcopy(self.start_state)
        pp.history = copy.deepcopy(self.history)
        pp.move_count = copy.copy(self.move_count)
        return pp
        
    def get_child(self):
        """build child of this One!"""
        res = []
        valid_moves = self.get_valid_moves()
        for move in valid_moves:
            child = self.deep_copy()
            child.apply_move(move)
            res.append(child)
        return res
    
    def print(self):
        """print self as int in Consule"""
        print(self.get_as_string() ,' MC: ' , self.move_count)
    def is_finished(self):
        """chech if is finished ? or not """
        for elemnt , gg  in zip( self.stattus , GOAL_ARRAY):
            if not (elemnt == gg):
                return False
        return True
    def gui_print(self):
        """print in Graphivall Whey !"""
        print(' _____ _____ _____ ')
        print('|     |     |     |')
        print('|  '+ self.stattus[0]+ '  |', end='' )
        print('  '+ self.stattus[1]+ '  |' ,end='' )
        print('  '+ self.stattus[2]+ '  |' )
        print('|_____|_____|_____|')
        print('|     |     |     |')
        print('|  '+ self.stattus[3]+ '  |', end='' )
        print('  '+ self.stattus[4]+ '  |' ,end='' )
        print('  '+ self.stattus[5]+ '  |' )
        print('|_____|_____|_____|')
        print('|     |     |     |')        
        print('|  '+ self.stattus[6]+ '  |', end='' )
        print('  '+ self.stattus[7]+ '  |' ,end='' )
        print('  '+ self.stattus[8]+ '  |' ) 
        print('|_____|_____|_____|')
                

# main ----------------------------------
def main():
    """define main as Seperated Function
    So i can collapse it (it take alot of space !)
    """
    print('Starting Main --------------------')
    start_time = time.time()
    solution = []
    
    p = puzzle()
    #p.cursor = 3;
    #p.stattus = [ '1', '2', '3','*', '4', '5', '6',  '7', '8']; 
    
    p.cursor = 0;
    p.stattus = [ '*', '1', '2','3', '4', '5', '6',  '7', '8'];
    
    old_key = '' #disable bouncing
    while True:
        key = keyboard.get_hotkey_name()
        if not old_key == key:
            os.system('cls' if os.name=='nt' else 'clear')
            if key == 'right' :  # if key 'q' is pressed 
                res = p.apply_move('R')
                if not res == False:
                    solution.append('L')
                #print('right pressed')
            if key == 'left':
                res = p.apply_move('L')
                if not res == False:
                    solution.append('R')
                #print('left pressed')
            if key == 'up':
                res = p.apply_move('U')
                if not res == False:
                    solution.append('D')
                #print('up pressed')
            if key == 'down':
                res = p.apply_move('D')
                if not res == False:
                    solution.append('U')
                #print('down pressed')
            
            old_key = key
            p.gui_print()
            print('hist: ' , p.history)
            print('fit : ' , p.get_fitnes())
            print('solution: ' , solution)
   
    
    print('took : ' , round((time.time() - start_time), 3)  , ' sec' )
    #end of main Function 
    
main()
