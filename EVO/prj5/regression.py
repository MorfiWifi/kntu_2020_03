"""
Created by MorfiWifi aka moteza eydipour (9811634)
KNT Univercity
2020
morfi dose'nt like copy ! [ so it's different !!]

How I would Solv the Proble is Abit weard
A(for + / - ) * x  + B * sin(Cx) + ... + H*x^2 + I*x^3 + J*x^4 ... also for x2
Tuning These Params will Make us Closer To aswer !
Some How They will Converg Randomly Because we Have noidea witch param is More important
and how to change them !
"""
import pandas as pd, time, numpy as np, matplotlib.pyplot as plt, copy, random

itarions = 1
generations = 1000
population_size = 20
ratio_limit = 1000
explort_max = 10  # size Of Sudden changes
explor_ratio = 0.9
best_ratio = 0.4
crossOver_ratio = 0.3  # Using Simple Average!
geensize = 14
validator = []  # this is Loaded data from Sample.TXT


# res =  np.sin(3.14/2)
# print("res :" , res)
# import random , numpy as np
def remove_redundency(input):
    """Gol bless Internet / its a good Thing !"""
    seen = set()
    seen_add = seen.add
    return [x for x in input if not (x in seen or seen_add(x))]


class Stats:
    """class for Saving Statistical Things
    as Shown in Their names"""

    def __init__(self, name, reps):
        self.size = name
        self.res = []
        self.maxes = []
        self.mins = []
        self.avgs = []
        self.finished_in = []
        self.failed_count = 0
        self.rep_count = reps
        self.took = []

    def print_stats(self):
        print(" --- RESULT FOR ", self.size, " REP= ", self.res, " --- ")
        print('MAXES : ', self.maxes)
        print('MINS : ', self.mins)
        print('AVG : ', self.avgs)
        print('Failed Count : ', self.failed_count)
        print('finished in rep : ', self.finished_in)
        print('TOOKS : ', self.took)
        print('res', self.res)


class Genom:
    def __init__(self, valuer_limit=1000, calc_error=False):
        # ratio range Considerd -1000 - +1000
        self.values = np.random.normal(size=(1, geensize)) * valuer_limit
        self.values = np.clip(self.values, -1 * ratio_limit, ratio_limit)
        self.error = 0
        # self.fitness = 0
        if calc_error:
            self.calc_error(validator)

    def get_fitnes(self):
        """ simple return off the fitness"""
        if self.error == 0:
            self.error = 0.001
        return 1 / self.error

    def is_same(self, g2):
        for i in range(geensize):
            if self.values[0][i] != g2.values[0][i]:
                return False
        return True

    def mutate(self):
        """with A chance will swap Two Genoms
        remember this mutation is Swap ONLY
        """
        wheele_value = random.randint(0, 100)
        if wheele_value < explor_ratio * 100:  # doo Mutation by 80%
            self.values = np.random.normal(size=(1, geensize)) * ratio_limit + self.values
            self.values = np.clip(self.values, -1 * ratio_limit, ratio_limit)

    def calc_error(self, validation):
        """calculate Eror Bsed On Given Data sample"""
        error = 0
        for x1, x2, y in validation:
            output = self.calc_output(x1, x2)
            error += (y - output) ** 2
        self.error = error
        return error

    def get_error(self):
        """just returns calculater previously calue"""
        return self.error

    def calc_output(self, x1, x2):
        """cal cilate out put base on in puts
        Zero Value Will Make Bad Thing's Happen !
        """
        if x1 == 0:
            x1 = 0.001
        if x2 == 0:
            x2 = 0.001
        return self.values[0][0] * x1 + self.values[0][1] * x2 \
               + self.values[0][2] * np.sin(x1) + self.values[0][3] * np.sin(x2) \
               + self.values[0][4] * np.cos(x1) + self.values[0][5] * np.cos(x2) \
               + self.values[0][6] * (x1 / x2) + self.values[0][7] * (x2 / x1) \
               + self.values[0][8] * x1 ** 2 + self.values[0][9] * x1 ** 3 + self.values[0][10] * x1 ** 4 \
               + self.values[0][11] * x2 ** 2 + self.values[0][12] * x2 ** 3 + self.values[0][13] * x2 ** 4

    def print_formula(self):
        print(
            "{:5.3f}*x1 + {:5.3f}*x2 + {:5.3f}*sin(x1) + {:5.3f}*sin(x2) + {:5.3f}*cos(x1) + {:5.3f}*cos(x2) + {:5.3f}*x1/x2 + {:5.3f}*x2/x1"
            "{:5.3f}*x1^2 + {:5.3f}*x1^3 + {:5.3f}*x1^4 + {:5.3f}*x2^2 + {:5.3f}*x2^3 + {:5.3f}*x2^4 +".format(
                self.values[0][0], self.values[0][1], self.values[0][2], self.values[0][3], self.values[0][4],
                self.values[0][5],
                self.values[0][6], self.values[0][7],
                self.values[0][8], self.values[0][9], self.values[0][10], self.values[0][11], self.values[0][12],
                self.values[0][13]))

    def __str__(self) -> str:
        return ' ER: ' + str(self.error)
        # return 'FT: ' + str(self.get_fitnes()) + ' ER: ' + str(self.error)


class Pool:
    def __init__(self, size=100):
        self.parents = []
        self.best = None
        self.size = size
        for i in range(size):
            self.parents.append(Genom(valuer_limit=ratio_limit, calc_error=True))

    def get_min_max_fitness(self):
        v_min = float('+inf')
        v_max = float('-inf')
        for i in range(len(self.parents)):
            # self.parents[i].save_finess_base()
            v_min = min(self.parents[i].get_fitnes(), v_min)
            v_max = max(self.parents[i].get_fitnes(), v_max)

        return [v_min, v_max]

    def get_best_genom(self):
        """:returns The best Genom from data (parent) in this pool instance"""
        return self.best

    def save_best_genom(self):
        """set best value to parent/child with best Fitness(max)"""
        self.sort_parents()
        self.best = self.parents[0]

    def sort_parents(self):
        """Just Sorts parets based on Fitness (max First)"""
        sorted_parents = []

        st_index = 0
        end_index = len(self.parents)
        while st_index < end_index:
            fitness = float("-inf")
            choosen_inedx = -1
            for i in range(len(self.parents)):
                gin = self.parents[i]
                # gin = Genom(5)

                if fitness < gin.get_fitnes():
                    fitness = gin.get_fitnes()
                    choosen_inedx = i

            sorted_parents.append(copy.deepcopy(self.parents[choosen_inedx]))
            self.parents.pop(choosen_inedx)  # remove this index
            st_index += 1

        self.parents = sorted_parents
        return sorted_parents  # just return STH

    def get_avg_fitness(self):
        value = 0.0  # cat ?
        for i in range(len(self.parents)):
            value = value + self.parents[i].get_fitnes()

        return value / len(self.parents)

    def select_new_generation_scaling(self):
        """selecting partens for being parent!
        based on Fitness With Scaling Method Over Total Finess
        """
        self.sort_parents()

        choosen_ones = []
        while len(choosen_ones) < self.size:
            choose_index = random.randint(0, len(self.parents))
            choose_index = choose_index % self.size  # Dont let Out Of range
            wheel = random.randint(0, 100) < 0.9 * 100
            if wheel:
                choosen_ones.append(copy.deepcopy(self.parents[choose_index]))

        self.parents = choosen_ones  # Only Use choosen

    def crossover_genoms(self, g1: Genom, g2: Genom) -> [Genom]:
        """:returns Two new Genomes Crossed Over !
        Too Complicated Way to do cross over I don't even know
        how could i find this solution!
        """
        cross_index = random.randint(0, geensize - 1)

        gg1 = Genom(ratio_limit)
        gg2 = Genom(ratio_limit)

        dd1 = []
        dd2 = []

        lg1 = g1.values[0][0:cross_index + 1]
        rg2 = g2.values[0][cross_index:geensize]
        rg1 = g1.values[0][cross_index:geensize]
        lg2 = g2.values[0][0:cross_index + 1]

        dd1.extend(lg1)
        dd1.extend(rg2)
        dd1.extend(rg1)
        dd1.extend(lg2)

        dd2.extend(lg2)
        dd2.extend(rg1)
        dd2.extend(rg2)
        dd2.extend(lg1)

        # dd1 = remove_redundency(dd1)
        # dd2 = remove_redundency(dd2)

        gg1.data = dd1
        gg2.data = dd2

        # Force Recalculate Cross Count
        gg1.calc_error(validator)
        gg2.calc_error(validator)

        return [gg1, gg2]

    def build_offsprings(self, hasMutation=False, age=0):
        """
        build offspring form Sorted parents (assume they are sorted)
        :return: Nothing
        """
        child = []
        born_count = 0

        while born_count < population_size:
            index1 = random.randint(0, population_size - 1)
            index2 = random.randint(0, population_size - 1)

            if index1 == index2:
                continue

            ch1 = self.parents[index1]
            ch2 = self.parents[index2]

            if ch1.is_same(ch2):
                ch1.mutate()

            two_child = self.crossover_genoms(ch1, ch2)
            child.extend(two_child)
            born_count += 2

        if hasMutation:  # in case Do Mutation / Works by chance!
            for boy in child:
                boy.mutate()

        for boy in child:
            boy.calc_error(validator)
        self.parents.extend(child)


def get_values_in_line(line):
    """seperate and Unique valies in line"""
    vals = []
    number = ''  # the readed number
    # for line in parts:
    for char in line:
        if char != " " and char != "," and char != "\n":
            number += char
        else:
            if number != '' and number != ' ' and number != '\n':
                vals.append(float(number))
            number = ''
            continue
    return vals


def main():
    status = Stats('Main Thread!', itarions)  # Object For saving Rsults ..

    for iteration in range(itarions):
        start_time = time.time()  # start Time -----
        poool = Pool(population_size)
        poool.sort_parents()  # cant say finished without sorting!
        avg = poool.get_avg_fitness()
        minmax = poool.get_min_max_fitness()

        for genn in range(generations):

            # if genn == 100:
            #     print('100 geen anniversary !')

            print('generation : ', str(genn), poool.best)
            poool.build_offsprings(hasMutation=True)
            poool.sort_parents()  # sort before choice
            poool.select_new_generation_scaling()  # this will make Some Re arangement!
            poool.sort_parents()  # sort Agin

            poool.save_best_genom()  # this Sorts parents too
            avg = poool.get_avg_fitness()
            minmax = poool.get_min_max_fitness()

        status.avgs.append(avg)
        status.maxes.append(minmax[1])
        status.mins.append(minmax[0])
        status.took.append((time.time() - start_time))

    status.print_stats()


# -----------------------main --------
start_time = time.time()
# Loading Samples to Ram ! (reading From File)
total_vals = []
file = open("sample.txt", "r")  # openning as text File
for line in file:
    vals = get_values_in_line(line)
    total_vals.append(vals)

validator = total_vals

main()

print('Took : ', (time.time() - start_time), 'sec')

