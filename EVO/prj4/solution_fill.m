%% Created by morfi Wifi AKA Morteza Eydipour (9811634)
%  As Always He Dosen't Like Copy So it's Difftent


%% 1: 
clc();
clear();
res  = es( 10 , ones(1,10) * -10 , ones(1,10) * 10 , 10000);
disp(res);

%% 2:



















%% FUNCTIONS (Main Function and ... Others)
function [xp, fp, stat] = es( n, lb, ub, stopeval)
% Strategy parameters
%...
    % Initialize
xp = lb;
    fp = -1;
    sigma = 1/ 15;
    evalcount = 0;
%Statistics administration
stat.name = '(1+1)-ES';
stat.evalcount = 0;
stat.histsigma = zeros(1, stopeval);
stat.histf = zeros(1, stopeval);
% Evolution cycle
    while evalcount < stopeval
        % Generate offspring and evaluate
        xo = xp + 1;
        %disp(xo)
            % generate offspring from parent xp
        fo = sphere(xo);
            % evaluate xo using fitnessfct
        evalcount = evalcount + 1;
        % select best and update success-rate and update 
        %stepsize
        % Important: MINIMIZATION!
        % Statistics administration
        %stat.histsigma = 
        % stepsize history
        %stat.histf =
        % fitness history
        % if desired: plot the statistics
    end
end


function f = sphere(x)
  f = sum(x.^2);
end
