"""
Created by MorfiWifi aka moteza eydipour (9811634)
KNT Univercity
2020
morfi dose'nt like copy ! [ so it's different !!]

this shall be a simple General Solution For Anything ?!
- I Guess yes!
1/5 leave from last generation
"""
import time, random, numpy as np, matplotlib.pyplot as plt


def avg(listi):
    return sum(listi) / len(listi)


def sphere(x):
    return sum(x[1:] ** 2)


def rosen(x):
    return sum(100.0 * (x[1:] - x[:-1] ** 2.0) ** 2.0 + (1 - x[:-1]) ** 2)


def ackley(x):
    n = len(x)
    f = 20 - 20 * np.exp(-0.2 * np.sqrt(sum(x ** 2, 2) / n)) - np.exp(sum(np.cos(2 * np.pi * x), 2) / n) + np.exp(1)
    return f


def fitness_func(x):
    return ackley(x)


def es(n, lb, ub, iterations):
    all_childs = []
    besties = []
    evalcount = 0
    xp = 0  # this is best
    g = 0.4  # global
    e = 0.1  # exploror
    p = np.random.random((n, n)) * (ub - lb)  # init parents

    best_fitness = float('+inf')
    for ch in p:
        fitness = fitness_func(ch)
        if fitness < best_fitness:
            xp = ch
            best_fitness = fitness

    all_childs.append(p)
    besties.append(xp)
    while evalcount < iterations:
        # build Off spring
        childs = []
        for parent in p:
            ch = (g * xp) + ((1 - g - e) * parent) + (e * random.random() * (ub - lb))
            childs.append(ch)

        # sort childs
        for i in range(len(childs) - 1):
            for j in range(i, len(childs)):
                if fitness_func(childs[i]) > fitness_func(childs[j]):
                    temp = childs[j]
                    childs[j] = childs[i]
                    childs[i] = temp

        # sort parents
        for i in range(len(p) - 1):
            for j in range(i, len(p)):
                if fitness_func(p[i]) > fitness_func(p[j]):
                    temp = p[j]
                    p[j] = p[i]
                    p[i] = temp

        # 1/5 of parents + 4/5 of child
        num_new_parents = int(n / 5)
        num_new_childs = n - num_new_parents

        new_parents = []
        for i in range(num_new_parents):
            new_parents.append(p[i])

        p = new_parents

        for i in range(num_new_childs):
            p.append(childs[i])

        # sort parents
        for i in range(len(p) - 1):
            for j in range(i, len(p)):
                if fitness_func(p[i]) > fitness_func(p[j]):
                    temp = p[j]
                    p[j] = p[i]
                    p[i] = temp

        # find best fitness
        for ch in p:
            fitness = fitness_func(ch)
            if fitness < best_fitness:
                xp = ch
                best_fitness = fitness

        besties.append(xp)
        evalcount += 1
    all_childs.append(p)

    print('best Fitness:', best_fitness)
    print('best child:', xp)
    return [all_childs, best_fitness, besties]


# main ---------------------------------------
start_time = time.time()  # start time

res = es(10, -10, 10, 10000)
# print('res:', res)
x = np.arange(0, 10001, 1).tolist()
y = []

for row in res[2]:
    y.append(avg(row))

plt.plot(x, y)
plt.xlabel('Generations')
plt.ylabel('AVG Fitness')
plt.title('Fitness Over Time (ackley)')
plt.show()

print('TOOK : ', '{:3.3f}'.format(time.time() - start_time), ' sec')
