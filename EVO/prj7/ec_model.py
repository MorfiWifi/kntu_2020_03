import numpy as np


def relu(Z):
    return np.maximum(0, Z)


def relu_backward(dA, cache):
    Z = cache
    dZ = np.array(dA, copy=True)
    dZ[Z <= 0] = 0
    return dZ


def sigmoid(z):
    return 1 / (1 + np.exp(-z))


def sigmoid_backward(dA, cache):
    Z = cache
    s = 1 / (1 + np.exp(-Z))
    dZ = dA * s * (1 - s)
    return dZ


def initialize_parameters():
    # Returns:
    #     parameters: initialized weights and biases of each layer
    # Implements:
    #     it just initializes the parameters of network's layers
    parameters = {}

    parameters['W1'] = np.random.randn(4, 2)*0.01
    parameters['b1'] = np.zeros((4, 1))

    parameters['W2'] = np.random.randn(4, 4)*0.01
    parameters['b2'] = np.zeros((4, 1))

    parameters['W3'] = np.random.randn(1, 4)*0.01
    parameters['b3'] = np.zeros((1, 1))

    return parameters


def linear_forward(A, W, b):
        # Arguments:
        #     A: result of the activation of the previous layer
        #     W: weights of the current layer
        #     b: bias of the current layer
        # Returns:
        #     Z: result of operations done on the input arguments
        #     cache: a tuple containing (A,W,b). we're gonna need them for backprop
        # Implements:
        #     a single forward pass

    Z = np.dot(W, A) + b
    cache = (A, W, b)

    return Z, cache


def linear_activation_forward(A_prev, W, b, activation):
    # Arguments:
    #     A_prev: the previous layer's activation value
    #     W: weights of the current layer
    #     b: bias of the current layer
    #     activation: a string which'll specify which activation function to use for this layer
    # Returns:
    #     A: the result of a single forward pass given the current arguments
    #     cache: a tuple containing the linear cache (A,W,b) and activation cache (Z). we'll need these for backprop
    # Implements:
    #     A single forward pass given the given activation function and arguments

    if activation == 'sigmoid':
        Z, linear_cache = linear_forward(A_prev, W, b)
        A, activation_cache = sigmoid(Z), Z
    elif activation == 'relu':
        Z, linear_cache = linear_forward(A_prev, W, b)
        A, activation_cache = relu(Z), Z

    cache = (linear_cache, activation_cache)

    return A, cache


def L_model_forward(X, parameters):
    # Arguments:
    #     X: networks inputs
    #     parameters: dictionary containing network's parameters
    # Returns:
    #     AL: the network's output given the input and parameters
    #     caches: a list containing the cache of each layer's calculations. we'll need them for backprop
    # Implements:
    #     A complete forward pass of inputs through the network

    caches = []
    A = X
    L = len(parameters) // 2

    for l in range(1, L):
        A_prev = A
        A, cache = linear_activation_forward(A_prev, parameters['W{0}'.format(l)], parameters['b{0}'.format(l)],
                                             activation='relu')
        caches.append(cache)
    AL, cache = linear_activation_forward(A, parameters['W{0}'.format(L)], parameters['b{0}'.format(L)],
                                          activation='sigmoid')

    caches.append(cache)

    return AL, caches


def compute_cost(AL, Y):
    # Arguments:
    #     AL: the output of the network
    #     Y: true labels of the inputs
    # Returns:
    #     cost: the cost of this iteration's calculations
    # Implements:
    #     network's cost function

    m = Y.shape[0]
    Y = Y.reshape(AL.T.shape)
    cost = (-1 / m) * np.sum(np.dot(Y, np.log(AL)) +
                             np.dot((1 - Y), np.log(1 - AL)))
    cost = np.squeeze(cost)

    return cost


def update_parameters(parameters, new_parameters):
    # Arguments:
    #     parameters: weights and biases of each layer
    #     grads: gradients calculated by backprop
    #     learning_rate: learning rate. you don't say!!!
    # Returns:
    #     updated parameters
    # Implements:
    #     updating network's parameters according to backprop calculations

    L = len(parameters) // 2

    for l in range(L):
        parameters["W" + str(l + 1)] = new_parameters["W" + str(l + 1)]
        parameters["b" + str(l + 1)] = new_parameters["b" + str(l + 1)]

    return parameters


def train(inputs, labels, parameters, new_parameters):
    # Arguments:
    #     inputs: network's inputs
    #     labels: labels of the inputs
    #     learning_rate:....
    # Returns:
    #     parameters: network's parameters after training
    # Implements:
    #     network's training given the arguments

    X = inputs
    Y = labels

    AL, caches = L_model_forward(X, parameters)
    cost = compute_cost(AL, Y)
    parameters = update_parameters(parameters, new_parameters)

    return parameters, cost


def predict(X, parameters):
    # Arguments:
    #     X: inputs
    #     parameters: trained network's parameters
    # Returns:
    #     networks predictions according to the given arguments
    # Implements:
    #     a single forward pass with the trained parameters

    prediction, cache = L_model_forward(X, parameters)

    return prediction
