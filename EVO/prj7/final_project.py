"""
This is Morfi wifi as always  aka morteza Eydipour (9811634)

I really don't know What to say / what todo ...
"""

# Importing Template Python File Given
import EVO.prj7.ec_model as ec
import time
import numpy as np
import matplotlib.pyplot as plt

def get_values_in_line(line):
    """seperate and Unique valies in line"""
    vals = []
    number = ''  # the readed number
    # for line in parts:
    for char in line:
        if char != " " and char != "," and char != "\n":
            number += char
        else:
            if number != '' and number != ' ' and number != '\n':
                vals.append(float(number))
            number = ''
            continue
    return vals


class Pool:
    """simple pool as Always
    this is all we have As GA algorithm !
    Noting More !!
    Other Thisg Considerd as User defined Objective and evaluatuion!!
    """

    def initiate(self, x, lables, parents_count):
        """Constructor for POOL
        :return Zipped Parent | Cost
        """
        # initialize population ----------------------------------
        parents = []
        costs = []
        for i in range(parents_count):
            instance = ec.initialize_parameters()
            instance, cost = ec.train(x, lables, instance, instance)
            parents.append(instance)
            costs.append(cost)

        zipped = zip(parents, costs)
        parent_cost = sorted(zipped, key=lambda x: x[1], reverse=False)

        # return paresnts , costs
        return parent_cost

    def order_parents(self, zipped_parents):
        parent_cost = sorted(zipped_parents, key=lambda x: x[1], reverse=False)
        return parent_cost

    def zipp_parents(self, parents, costs):
        return zip(parents, costs)

    def unzipp_parents(self, zipped_parents):
        parens = []
        costs = []

        for p, c in zipped_parents:
            parens.append(p)
            costs.append(c)

        return parens, costs

    def cross_over(self, zp_parents, cross_index, shapes):
        """Cross over on parents (how many resulst you want? = size)"""
        offs = []
        parents = []
        for parent, cost in zp_parents:
            parents.append(parent)

        rands = np.random.randint(0, len(parents), (1, 2))
        p1 = parents[rands[0][0]]
        p2 = parents[rands[0][1]]

        off1 = {}
        off2 = {}
        for p_n, p_s in shapes:
            liniersize = p_s[0] * p_s[1]

            indexes = np.uint8(int(liniersize * cross_index))

            p1_param_lin = np.reshape(p1[p_n], (liniersize, 1))
            p2_param_lin = np.reshape(p2[p_n], (liniersize, 1))

            off1[p_n] = np.empty((1, liniersize))
            appended1 = np.append(p1_param_lin[0:indexes], p2_param_lin[indexes:])
            off1[p_n] = appended1

            off2[p_n] = np.empty((1, liniersize))
            appended2 = np.append(p2_param_lin[0: indexes], p1_param_lin[indexes:])
            off2[p_n] = appended2

        offs.append(off1)
        offs.append(off2)

        return offs

    def mutate(self, offsprings, p_names, p_shapes):
        """mutation fially!"""
        mutated_offs = []
        for offspring in offsprings:
            off = {}
            for p_name, p_shape in zip(p_names, p_shapes):
                val = offspring[p_name]
                lin_param = np.reshape(val, (p_shape[0] * p_shape[1], 1))
                random_value = np.random.uniform(mutate_lb, mutate_ub, (p_shape[0] * p_shape[1], 1))
                lin_param = np.add(lin_param, random_value)
                off[p_name] = np.reshape(lin_param, p_shape)

            mutated_offs.append(off)

        return mutated_offs


param_names = ['W1', 'b1', 'W2', 'b2', 'W3', 'b3']
param_sizes = [(4, 2), (4, 1), (4, 4), (4, 1), (1, 4), (1, 1)]
mutate_lb = -2
mutate_ub = 2

param_shape = zip(param_names, param_sizes)
# This is Main ----------
sTime = time.time()

inputs = []  # load from file
validation_set = []  # load from file

# read Param's and Result samples from file
total_vals = []
file = open("sample.txt", "r")  # openning as text File
for line in file:
    vals = get_values_in_line(line)
    inputs.append([vals[0], vals[1]])
    validation_set.append([vals[2]])

# Choosing 2 Line of Inputs and Validation set!!
# I Don't Know Why ec dose'nt take all validations and input!
inputs = np.array(inputs[0:2])
# inputs = np.array(inputs)
validation_set = np.array(validation_set[0:2])
# validation_set = np.array(validation_set)


params = ec.initialize_parameters()
# print("params : ", params)

new_params = []

params, cost = ec.train(inputs, validation_set, params, params)

#  Evo starts here ---------------------------------------
parent_size = 100
num_generations = 100
pool = Pool()
zipped_parent_cost = pool.initiate(inputs, validation_set, parent_size)
baest_cost = []

# iterate as generations you need -------
for generation in range(num_generations):
    print("generation :", generation)
    # params, cost = ec.train(inputs, validation_set, params, params)
    # ziped_parent_cost = pool.order_parents(ziped_parent_cost)

    parents, costs = pool.unzipp_parents(zipped_parent_cost)
    # print(parents)
    print('best cost :', costs[0])
    baest_cost.append(costs[0])

    # print("parents Len :", str(len(parents)))
    # print("zip_parents Len :", str(len(zipped_parent_cost)))
    # cross Over Center! (build 2 child)
    offspring_crossover = pool.cross_over(zip(parents, costs), 0.5, zip(param_names, param_sizes))
    # Mutate
    offspring_mutation = pool.mutate(offspring_crossover, param_names, param_sizes)

    # parents, costs = pool.unzipp_parents(zipped_parent_cost)

    for off in offspring_mutation:
        offi, cost = ec.train(inputs, validation_set, off, off)
        parents.append(off)
        costs.append(cost)

    zipped_parent_cost = zip(parents, costs)

    zipped_parent_cost = sorted(zipped_parent_cost, key=lambda x: x[1], reverse=False)

    parents, costs = pool.unzipp_parents(zipped_parent_cost)

    # remove Two Worth
    parents.pop()
    parents.pop()
    costs.pop()
    costs.pop()

    zipped_parent_cost = zip(parents, costs)
    # At this point parents are Ordered
    # print('best cost :',costs[0])

# print(cost)
parents, costs = pool.unzipp_parents(zipped_parent_cost)
print("best Cost:", costs[0])
print("best weights:", parents[0])
baest_cost.append(costs[0])

eTime = time.time()
print("OPERATION TOOK : ", eTime - sTime, "Sec")

fig, ax = plt.subplots()
ax.plot([x for x in range(len(baest_cost))], baest_cost)
plt.show()
