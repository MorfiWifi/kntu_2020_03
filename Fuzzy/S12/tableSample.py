"""
Created by Morfi wifi AKA Morteza Eydiour 9811634
KNTU University 2020
"""
x1=input(['please enter data of x1 input:','\n','x1=']);
x2=input(['please enter data of x2 input:','\n','x2=']);
y=x1.^2+x1*x2;
ss=size(x1);
num=ss(2);
for i=1:num
     b=[.75 1 1.25];
     for j=1:5
         s(j)=trimf(x1(i),b);
         b=b+.25;
         if j==1
         s(j)=trimf(x1(i),[1 1 1.25]);
         end
         if j==5
         s(j)=trimf(x1(i),[1.75 2 2]);
         end
    end
    [m n]=max(s);
    g(4,i)=m;
    g(1,i)=n;
end
for i=1:num
    b=[.8 1 1.2];
    for j=1:6
         s(j)=trimf(x2(i),b);
         b=b+.2;
         if j==1
         s(j)=trimf(x2(i),[1 1 1.2]);
         end
         if j==6
         s(j)=trimf(x2(i),[1.8 2 2]);
         end
    end
    [m n]=max(s);
    g(4,i)=m*g(4,i);
    g(2,i)=n;
end
for i=1:num
    b=[1 2 3];
    for j=1:7
         s(j)=trimf(y(i),b);
         b=b+1;
         if j==1
         s(j)=trimf(y(i),[2 2 3]);
         end
         if j==7
         s(j)=trimf(y(i),[7 8 8]);
        end
    end
    [m n]=max(s);
    g(3,i)=n;
    g(4,i)=m*g(4,i);
end

%second part
n=num;
for i=1:n
    if i==n
       break;
    end
    for j=i+1:n
        if g(1,i)==g(1,j)&g(2,i)==g(2,j)
           if g(4,i)>=g(4,j)
              g(:,j)=0;
           else
              g(:,i)=g(:,j);
              g(:,j)=0;
           end
        end
    end
end
i=1;
while i<=n
     if g(:,i)==0
        g(:,i)=[];
        n=n-1;
        i=i-1;
     end
     i=i+1;
end
g(4,:)=[];
c=g(3,:);
%change third row of G to output fuzzy sets center
g(3,:)=g(3,:)+1;
g
[x1,x2]=meshgrid(1:.01:2,1:.01:2);
r=size(g);
a=[1 1 1.25;1 1.25 1.5;1.25,1.5,1.75;1.5,1.75,2;1.75,2,2];
b=[1 1 1.2;1 1.2 1.4;1.2 1.4 1.6;1.4 1.6 1.8;1.6 1.8 2;1.8 2 2];
sum1=0;sum2=0;
for i=1:r(2)
    s1=trimf(x1,a(g(1,i),:));
    s2=trimf(x2,b(g(2,i),:));
    sum1=s1.*s2.*g(3,i)+sum1;
    sum2=s1.*s2+sum2;
end
y1=sum1./sum2;
%surfl(x1,x2,y);
shading interp
colormap(gray);
hold on;
y=x1.^2+x1.*x2;
surf(x1,x2,y-y1)
