"""
Created by Morfi wifi AKA Morteza Eydiour 9811634
KNTU University 2020
"""
import numpy as np


def truck(input_oriantation, x):
    """calculate the Wheel Rotation at a poiot"""
    total_local = 0
    total_global = 0
    location_matrix = [
        [-23, -50, 0, 0, 0],
        [-23, -50, -50, -50, -50],
        [12, -12, -23, -50, -23],
        [23, 23, 0, -23, -23],
        [23, 50, 23, 12, -12],
        [0, 50, 50, 50, 23],
        [0, 0, 0, 50, 23]
    ]

    validity = np.ones((7, 5))
    validity[0] = [1, 1, 0, 0, 0]
    validity[5] = [0, 1, 0, 0, 0]
    validity[6] = [0, 0, 0, 1, 1]

    angle = -45
    for i in range(7):
        if abs(input_oriantation-angle) <= 45:
            radius = 1-(abs(input_oriantation-angle)/45)
        else:
            radius = 0
        angle = angle+45
        distance = 2
        for j in range(5):
            if abs(x-distance) <= 4:
                sumi = 1-( abs( x - distance ) / 4)
            else:
                sumi = 0
            distance = distance+4
            const = radius* sumi * location_matrix[i][j]
            total_local = total_local + const
            calc = radius * sumi * validity[i][j]
            total_global = total_global + calc

    return total_local/total_global


res = truck(5, 6)
print(res)
